const config = require('../config')()

describe('MongoDB"', () => {
  it('is there a server running', (next) => {
    const MongoClient = require('mongodb').MongoClient
    MongoClient.connect(`mongodb://${ config.mongo.host }:${ config.mongo.port }/somethingnew`, (err, db) => {
      expect(err).toBe(null)
      next()
    })
  })
})
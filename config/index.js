const config = {
  local: {
    mongo: {
      host: '127.0.0.1',
      port: 27017,
    },
    mode: 'local',
    port: 3000,
  },
  staging: {
    mode: 'staging',
    port: 4000,
  },
  production: {
    mode: 'production',
    port: 5000,
  }
}

module.exports = (mode) => {
  return config[mode || process.argv[2] || 'local'] || config.local
}
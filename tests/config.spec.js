describe('Configuration setup', () => {
  it('should load the configuration', (next) => {
    const config = require('../config')()
    expect(config.mode).toBe('local')
    next()
  })

  it('should load the staging config', (next) => {
    const config = require('../config')('staging')
    expect(config.mode).toBe('staging')
    next()
  })

  it('should load production config', (next) => {
    const config = require('../config')('production')
    expect(config.mode).toBe('production')
    next()
  })
})
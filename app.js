const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const lessMiddleware = require('less-middleware');
const http = require('http')

const MongoClient = require('mongodb').MongoClient

const index = require('./routes/index');
const users = require('./routes/users');

const config = require('./config')()

const app = express();

let globalDB
const attachDBMiddleware = (req, res, next) => {
  req.db = globalDB
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', attachDBMiddleware, index);
app.use('/users', attachDBMiddleware, users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

MongoClient.connect(`mongodb://${ config.mongo.host }:${ config.mongo.port }/somethingnew`, (err, db) => {
  if (err) 
    return console.err('No mongodb server is running')

  globalDB = db

  http.createServer(app).listen(config.port, () => {
    console.log('Listening on port ' + config.port)
  })

})

module.exports = app